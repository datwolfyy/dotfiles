# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias x='startx'
alias ls='ls --color=auto'

# nge theme: 38;5;166
# cthulu theme: 38;5;30
PS1='\[\e[38;5;166;1m\]\u\[\e[m\]@\[\e[38;5;88;1m\]\h\[\e[m\]:\W \[\e[1m\]\$\[\e[m\] '

export EDITOR="vim"
export PATH="${PATH}:${HOME}/.local/bin"
